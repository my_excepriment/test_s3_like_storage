from pydantic import BaseModel


class InfoObjectLikeS3(BaseModel):
    bucket_name: str
    object_name: str
