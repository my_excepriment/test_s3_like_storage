import pathlib
import pytest
from repository.s3_like_repo import S3LikeRepo, get_s3_like_repo
from schemas.base import InfoObjectLikeS3


@pytest.fixture(scope='session')
def s3_like_repo() -> S3LikeRepo:
    return get_s3_like_repo()


@pytest.fixture(scope='session')
def s3_list_all_object(
    upload_files: None, s3_like_repo: S3LikeRepo
) -> list[InfoObjectLikeS3]:
    return s3_like_repo.get_all_object()


@pytest.fixture(scope='session')
def path_to_test_data() -> pathlib.PosixPath:
    return pathlib.Path(__file__).parents[2] / 'test_data'


@pytest.fixture(scope='session')
def upload_files(
    s3_like_repo: S3LikeRepo,
    bucket_name_exist: str,
    path_to_test_data: pathlib.PosixPath,
) -> None:
    s3_like_repo.bucket_delete_if_exist(bucket_name=bucket_name_exist)
    s3_like_repo.create_bucket(bucket_name=bucket_name_exist)
    _recursively_upoad_files(
        s3_like_repo=s3_like_repo,
        path_to_test_data=path_to_test_data,
        bucket_name=bucket_name_exist,
    )


def _recursively_upoad_files(
    s3_like_repo: S3LikeRepo,
    path_to_test_data: pathlib.PosixPath,
    bucket_name: str,
) -> None:
    for item in path_to_test_data.rglob('*'):
        if item.is_file():
            relative_path = str(item.relative_to(path_to_test_data))
            s3_like_repo.upload_file(
                bucket_name=bucket_name,
                path_to_file=item,
                key_file=str(relative_path),
            )


#        s3_like_repo.upload_file(bucket_name=bucket_name, path_to_file='test')


@pytest.fixture(scope='session')
def bucket_name_exist() -> str:
    return 'test-files'


@pytest.fixture(scope='session')
def bucket_name_not_exist() -> str:
    return 'not-exist-bucket'
