from repository.s3_like_repo import S3LikeRepo
from schemas.base import InfoObjectLikeS3


class TestBasic:
    def test_basic(
        self,
        s3_like_repo: S3LikeRepo,
        s3_list_all_object: list[InfoObjectLikeS3],
    ) -> None:
        object_info = s3_list_all_object[0]
        presigned_url = s3_like_repo.get_presigned_url(
            bucket_name=object_info.bucket_name,
            object_name=object_info.object_name,
        )

        assert presigned_url


class TestBuckets:
    def test_get_bucket_exist(
        self,
        s3_like_repo: S3LikeRepo,
        s3_list_all_object: list[InfoObjectLikeS3],
        bucket_name_exist: str,
    ) -> None:
        bucket = s3_like_repo._get_bucket_by_name(bucket_name=bucket_name_exist)

        assert bucket is not None

    def test_get_bucket_not_exist(
        self,
        s3_like_repo: S3LikeRepo,
        s3_list_all_object: list[InfoObjectLikeS3],
        bucket_name_not_exist: str,
    ) -> None:
        bucket = s3_like_repo._get_bucket_by_name(
            bucket_name=bucket_name_not_exist
        )

        assert bucket is None

    def test_bucket_exist_with_exist_name(
        self,
        s3_like_repo: S3LikeRepo,
        s3_list_all_object: list[InfoObjectLikeS3],
        bucket_name_exist: str,
    ) -> None:
        assert s3_like_repo._bucket_is_exist(bucket_name=bucket_name_exist)

    def test_bucket_exist_with_not_exist_name(
        self,
        s3_like_repo: S3LikeRepo,
        s3_list_all_object: list[InfoObjectLikeS3],
        bucket_name_not_exist: str,
    ) -> None:
        assert not s3_like_repo._bucket_is_exist(
            bucket_name=bucket_name_not_exist
        )
