from datetime import date
from http import HTTPStatus
import logging
import pathlib
from typing import Any

import boto3
import mypy_boto3_s3
from core import configs
from botocore.client import ClientError

from schemas.base import InfoObjectLikeS3


class S3LikeRepo:
    _logger: logging.Logger
    _default_exp_in_sec: int | None
    _client: mypy_boto3_s3.client
    _resources: mypy_boto3_s3.service_resource

    def __init__(
        self,
        access_key: str,
        secret_key: str,
        host: str,
        port: int,
        default_exp_in_sec: int | None = None,
    ) -> None:
        self._logger = logging.getLogger(__name__).getChild(type(self).__name__)
        self._default_exp_in_sec = default_exp_in_sec
        try:
            dict_data_connect = {
                'endpoint_url': f'http://{host}:{port}',
                'aws_access_key_id': access_key,
                'aws_secret_access_key': secret_key,
            }
            self._client = boto3.client('s3', **dict_data_connect)
            self._resources = boto3.resource('s3', **dict_data_connect)
            self._logger.debug('Connect to s3 storage is successfully')
        except Exception as exc_info:
            self._logger.exception(
                msg='Error connect to s3 storage', exc_info=exc_info
            )

    def get_all_object(self) -> list[InfoObjectLikeS3]:
        response = self._client.list_buckets()
        list_response = []
        for bucket in response['Buckets']:
            name_bucket = bucket.get('Name')
            for object in self._client.list_objects(Bucket=name_bucket)[
                'Contents'
            ]:
                list_response.append(
                    InfoObjectLikeS3(
                        bucket_name=name_bucket, object_name=object.get('Key')
                    )
                )

        return list_response

    def get_presigned_url(
        self, bucket_name: str, object_name: str, exp_in_sec: int | None = None
    ) -> str:
        dict_data = {}
        if exp_in_sec is None:
            if self._default_exp_in_sec is not None:
                dict_data['ExpiresIn'] = self._default_exp_in_sec
        else:
            dict_data['ExpiresIn'] = exp_in_sec
        try:
            link = self._client.generate_presigned_url(
                'get_object',
                Params={'Bucket': bucket_name, 'Key': object_name},
                **dict_data,
            )
        except Exception as exc_info:
            self._logger.exception(
                msg='Error get presigned link', exc_info=exc_info
            )
            return None

        return link

    def bucket_delete_if_exist(self, bucket_name: str) -> bool:
        if self._bucket_is_exist(bucket_name=bucket_name):
            bucket = self._get_bucket_by_name(bucket_name=bucket_name)
            bucket.objects.all().delete()
            self._client.delete_bucket(Bucket=bucket_name)

            return True

        return False

    def _bucket_is_exist(self, bucket_name: str) -> bool:
        try:
            self._client.head_bucket(Bucket=bucket_name)
        except ClientError:
            return False

        return True

    def _get_bucket_by_name(
        self, bucket_name: str
    ) -> mypy_boto3_s3.service_resource.Bucket | None:
        bucket = self._resources.Bucket(bucket_name)
        if bucket.creation_date is None:
            return None

        return bucket

    def create_bucket(self, bucket_name: str) -> bool:
        try:
            response = self._client.create_bucket(
                Bucket=bucket_name,
            )
        except Exception as exc_info:
            self._logger.exception(
                msg=f'Error create bucket {bucket_name}', exc_info=exc_info
            )

            return False

        return response['ResponseMetadata']['HTTPStatusCode'] == HTTPStatus.OK

    def upload_file(
        self,
        bucket_name: str,
        path_to_file: pathlib.PosixPath,
        key_file: str | None = None,
    ) -> bool:
        bucket = self._get_bucket_by_name(bucket_name=bucket_name)
        if key_file is None:
            key_file = path_to_file.name
        try:
            bucket.upload_file(Filename=str(path_to_file), Key=key_file)
        except Exception as exc_info:
            self._logger.exception(
                msg=f'Error upload file, path to file {str(path_to_file)}',
                exc_info=exc_info,
            )
            return False

        return True


s3_like_repo: S3LikeRepo | None = None


def get_s3_like_repo() -> S3LikeRepo:
    global s3_like_repo
    if s3_like_repo is None:
        s3_like_repo = S3LikeRepo(
            **configs.min_io_config.get_credential(),
        )
    return s3_like_repo
