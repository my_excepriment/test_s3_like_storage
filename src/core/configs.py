# Корень проекта
import pathlib

from dotenv import load_dotenv
from pydantic_settings import BaseSettings, SettingsConfigDict


ROOT_DIR_SRC_API = pathlib.Path(__file__).parents[2]
_base_to_env = ROOT_DIR_SRC_API.joinpath('dc', 'dev', '.env')
if _base_to_env.exists():
    load_dotenv(_base_to_env)


class MyBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file='.env', env_file_encoding='utf-8'
    )


class MinIO(MyBaseSettings):
    model_config = SettingsConfigDict(env_prefix='MINIO_')

    root_user: str
    root_password: str
    access_key: str
    secret_key: str
    host: str
    port: int

    def get_credential(self) -> dict:
        return {
            'access_key': self.access_key,
            'secret_key': self.secret_key,
            'host': self.host,
            'port': self.port,
        }


min_io_config: MinIO = MinIO()
